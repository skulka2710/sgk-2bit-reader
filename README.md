# README #

This repository contains code for a very simple IGB App that reads
and summarizes information about a TwoBit sequence file. 

Adds a new menu item to the Tools menu. 

Main goals of this App:

* Show how to embed non-OSGI JARs in an IGB App.
* Show how to open, read, and summarize data in a 2bit sequence file. 

### How do I get set up? ###

* Clone the project. 
* Open the project in NetBeans.
* Build it (using Maven).
* Start IGB (version 9 or higher, less than 10).
* Within IGB, use the App Manager (Tools menu) to add project target directory as a new App Repository.
* Note the App appears in the App Manager. Use the App Manager to install it. Installing the App adds a new menu item to the Tools menu in IGB.
* Go back to IGB. Select **Tools > Summarize TwoBit File** to run the App.

### Questions or Comments? ###

Contact:

* Ann Loraine - aloraine@uncc.edu or aloraine@gmail.com
* Deepti Joshi - djoshi4@uncc.edu
